const logo = {
    title: 'nike'
}
const navBarItems = [
    {
        title: 'WOOCOMMERCE  PRODUCT  SLIDER'
    },
    {
        title: 'CART (0)'
    }
]


const images = [
    {
      logo: "src/assets/icons/nike.svg",
    },
    {
      twitter: "src/assets/icons/twitter.svg",
    },
    {
      instagram: "src/assets/icons/instagram.svg",
    },
    {
      facebook: "src/assets/icons/facebook.svg",
    },
    {
      shoes: "src/assets/images/shoes.svg",
    },
    {
      star: "src/assets/icons/star.svg",
    },
    {
      emptyStar: "src/assets/icons/emptyStar.svg",
    },
    {
      arrow: "src/assets/icons/arrow.svg",
    },
  ];

  
  const description = {
    title:
      "A Woocommerce product gallery Slider for Slider Revolution with mind-blowing visuals.",
  };
  
  const price = {
    price: "$749",
  };
  
  const button = {
    CTA: "BUY NOW",
  };
  
  const heading = {
    title: "NIKE CRYPTOKICK",
  };
  
  const cart = {
    items: "0",
  };
  
  
  export function getImages() {
    return [...images];
  }
  
  export function getDescription() {
    return { ...description };
  }
  
  export function getButton() {
    return { ...button };
  }
  
  export function getPrice() {
    return { ...price };
  }
  
  export function getHeading() {
    return { ...heading };
  }
  
  export function getCart() {
    return { ...cart };
  }
  
export function getNavBarItems() {
    return [... navBarItems];
}