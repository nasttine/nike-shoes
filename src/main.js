import "./style.css";
import {setupHeader} from './components/header/header'
import { setupMain } from "./components/main-part/main-part";
import { setupFooter } from "./components/footer/footer";
import { getNavBarItems, getImages, getHeading, getPrice, getDescription, getButton, getCart
   } from "./api/api";

   setupHeader(
    document.getElementById("app"), {
    navBarItems: getNavBarItems(),
    images: getImages(),
    cart: getCart(),
  });
  
  setupMain(document.getElementById("app"), {
    images: getImages(),
    heading: getHeading(),
  });
  
  setupFooter(document.getElementById("app"), {
    images: getImages(),
    price: getPrice(),
    description: getDescription(),
    button: getButton(),
  });
  
  
